# HQ Dash

Smashing for teh HQ Dashboard

## Requirements

- ruby >= 2.4
- smashing (most recent version)
	- `gem install smashing`

## Install

- `git clone https://chaos.expert/ccc-ffm/dashboard.git`
- `cd dashboard/src`
- `bundle`
- `cp jobs/twitter_config.rb jobs/twitter_local_config.rb`
- `cp jobs/publictransport_config.rb jobs/publictransport_local_config.rb`
- `cp config_config.rb jobs/config_local_config.rb`
- edit <jobname>_local_config.rb
- `cp ../systemd/dashing.service /etc/systemd/system/dashing.service`
- edit /etc/systemd/system/dashing.service to match your needs
- `systemctl daemon-reload`
- done

## Run
`systemctl start dashing`

To autostart dashing at boot time run the following command: `systemctl enable dashing`
