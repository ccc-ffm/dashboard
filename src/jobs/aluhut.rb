require 'mqtt'

# Populate the graph with some random points
points = []
(1..10).each do |i|
	points << { x: i, y: 0 }
end
last_x = points.last[:x]


SCHEDULER.every '5m', :allow_overlapping => false, :first_in => 0 do |job|
	MQTT::Client.connect('mqtt://mqtt.hq') do |client|
    	client.subscribe("hq/alarm/aluhut")
    	client.get do |topic,message|
    		p topic
    		
	        message = message.force_encoding('utf-8')
	        value = message.to_f
	        points << { x: last_x, y: value }

	        points.shift
			last_x += 1
			p points
			send_event('aluhutlevel', points: points)
		end
	end
end
