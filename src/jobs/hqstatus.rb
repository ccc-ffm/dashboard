require 'mqtt'

# Set your MQTT server
MQTT_SERVER = 'mqtt://mqtt.cccffm.space'

# Set the MQTT topics you're interested in and the tag (data-id) to send for dashing events
MQTT_TOPICS = { 'hq/status' => 'hqstatus',
                'hq/electricity/power/hq' => 'hqpower',
                'hq/network/wifi/donotuse/dhcp/clients' => 'wifidhcpclients',
                'hq/network/router/internet/rx' => 'internetrx',
                'hq/network/router/internet/tx' => 'internettx',
              }

# Start a new thread for the MQTT client
Thread.new {
  begin
    MQTT::Client.connect(MQTT_SERVER) do |client|
      client.subscribe( MQTT_TOPICS.keys )

      # Sets the default values to 0 - used when updating 'last_values'
      current_values = Hash.new(0)

      client.get do |topic,message|
        tag = MQTT_TOPICS[topic]
        last = current_values[tag]
        if ['internetrx', 'internettx'].include? tag
          value = message.to_i
          if value >= 1073741824
            value = (value / 1073741824).round(2)
            unit = 'GB/s'
          elsif value >= 1048576
            value = (value / 1048576.0).round(2)
            unit = 'MB/s'
          elsif value >= 1024
            value = (value / 1024.0).round(2)
            unit = 'kB/s'
          else
            value = value
            unit = 'B/s'
          end
          # puts value
          send_event(tag, {text: value, unit: unit})
        elsif tag != 'hqstatus'
          current_values[tag] = message.to_i
          difference = current_values[tag] - last
          send_event(tag, {value: current_values[tag], difference: difference, text: current_values[tag]})
        else 
          # hqstatus
          send_event(tag, hqstatus: message.downcase)
        end
      end
    end
  rescue
    puts 'MQTT-Server nicht erreichbar'
  end
}
