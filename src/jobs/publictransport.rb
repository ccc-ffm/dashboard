require 'net/http'
require 'json'
require 'uri'
require 'date'
require_relative 'publictransport_local_config.rb'


cnfobj = PublicTransport.new
config = cnfobj.config

APIURL = config['APIBASEURL']
def get_departures(apiurl, stationid, offset, profile, products)

	start = Time.now
	if offset > 0
		start += offset*60
	end

	searchTime = start.strftime("%Y-%m-%dT%H:%M:%S")
	departures = []
	uri = "#{apiurl}?station=%s&profile=%s&date=%s" % [stationid, profile, searchTime]
	p uri
	begin
		uri = URI.parse(uri)
		http = Net::HTTP.new(uri.host, uri.port)
		http.use_ssl = true
		request = Net::HTTP::Get.new(uri)
		response = http.request(request)
		data = JSON.parse(response.body)
		ret = []
		data.each do |departure|
			tmp = {}		
			departureUniversalTime = Time.parse(departure['departure']['time'])
			departureLocalTime = departureUniversalTime.getlocal
			departureTime = departureLocalTime.strftime("%H:%M")

		
			if departure['departure']['delay'] != 0
				tmp['time'] = "<span class=\"delayed\">%s</span>" % [departureTime]
				tmp['delayed'] = "delayed"
			else
				tmp['time'] = departureTime
				tmp['delayed'] = false
			end

			# destination (direction)
			dest = departure['finalDestination']
			dest.slice! "Frankfurt (Main)"
			dest.slice! " Bahnhof"
			dest.slice! ", Frankfurt a.M."
			dest = dest.sub("Hauptbahnhof", "Hbf")
			if dest.strip! == "Hbf"
				dest = "Frankfurt Hbf"
			end
			tmp['dest'] = dest

			# line formatting
			span = "<span class=\"%s\">%s%s</span>"
			cat = departure['train']['type'].strip
			number = departure['train']['name'].strip
			line = departure['train']['line'].strip

			if not products.include?(cat.downcase)
				next
			end

			case cat.downcase
				when "bus"
					tmp['line'] = span % ["bus", line, ""]
				when "u-bahn"
					tmp['line'] = span % ["subway", line, ""]
				when "s"
					tmp['line'] = span % ["suburban", "S", line]
				when "rb"
					tmp['line'] = span % ["regio", "RB", line]
				when "re"
					tmp['line'] = span % ["regio", "RE", line]
				when "ic"
					tmp['line'] = span % ["ic", "", number]
				when "ice"
					tmp['line'] = span % ["ice", "", number]
				when "str"
					tmp['line'] = span % ["tram", "", line]
			end

			ret.push(tmp)
			if ret.length >= 10
				break
			end
		end
		return ret
	rescue
		puts "\e[31mSomething went wrong while querying the API\e[0m"
	end
end

SCHEDULER.every '1m', :first_in => 0 do
	## Busse Schönhof
	dep_sh = get_departures(APIURL, "3001206", 1, "rmv", ['bus', 'nightbus'])
	dep_kp = get_departures(APIURL, "3001234", 3, "rmv", ['u-bahn'])
	dep_wb = get_departures(APIURL, "101204", 5, "db", ['s', 'rb', 're', 'ic', 'ice'])
	dep_wt = get_departures(APIURL, "101204", 5, "db", ['bus', 'nightbus', 'str'])
	send_event('busfahrplan', departures: dep_sh)
	send_event('ubahnfahrplan', departures: dep_kp)
	send_event('bahnfahrplan', departures: dep_wb)
	send_event('tramfahrplan', departures: dep_wt)
end

# ID Haltestelle Schönhof:     003001234
# ID Haltestelle Kirchplatz:   003001206
# ID Haltestelle Westbahnhof:  003001204

# 64 = Bus
# 16 = U-Bahn
# 31 = Fernzuege, Regio, S-Bahn
