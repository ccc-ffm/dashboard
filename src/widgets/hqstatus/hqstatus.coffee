class Dashing.Hqstatus extends Dashing.Widget

  color: () ->
    status = @get('hqstatus')
    switch status
      when "open" then "#82BF6E"
      when "private" then "#f3a32a"
      when "closed" then "#E94858"
      else "#737173"

  onData: (data) ->
    $(@node).css('background-color', @color())